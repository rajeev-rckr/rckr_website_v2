jQuery(document).ready(function ($) {
  var headerHeight = 70;
  var position = jQuery(window).scrollTop();
  $(window).scroll(function () {
    var scroll = $(this).scrollTop();
    $("body")
      .toggleClass("header-popup", scroll > headerHeight)
      .toggleClass(
        "hide-header-popup",
        scroll > position && scroll > headerHeight
      )
      .toggleClass("header-popup-show", scroll <= position && scroll > 0)
      .toggleClass("normal-header", scroll <= position && scroll === 0);
    position = scroll;
  });

  function animateValue(obj, start, end, duration) {
    let startTimestamp = null;
    const step = (timestamp) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      obj.innerHTML = Math.floor(progress * (end - start) + start);
      if (progress < 1) {
        window.requestAnimationFrame(step);
      }
    };
    window.requestAnimationFrame(step);
  }

  $(".contactus-form .form-element").focus(function () {
    $(this)
      .parent()
      .addClass("active")
      .focusout(function () {
        $(this)
          .parent()
          .toggleClass("active", $(this).val() != "");
      });
  });

  function requestJson(callback, path) {
    $.getJSON(path, callback).fail(function () {
      console.error("Failed to load JSON from " + path);
    });
  }

  if (location.pathname.endsWith("/")) {
    animateStats();
  }

  function animateStats() {
    var animated = false;
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting && !animated) {
          ["years", "clients", "retention", "products", "locations"].forEach(
            (id, index) => {
              animateValue(
                document.getElementById(id),
                0,
                [20, 98, 93, 100, 2][index],
                [1000, 1000, 1000, 1500, 500][index]
              );
            }
          );
          animated = true;
        }
      },
      { threshold: 0.4 }
    );
    observer.observe(document.getElementById("years"));
  }

  if (location.pathname.endsWith("careers.html")) {
    requestJson(function (data) {
      var _index = new Date().getUTCHours();
      var populationLimit = data.values[_index][_index].split(",")[0];
      document.getElementById("population_limit").innerHTML = populationLimit;
      var openings_div = $("#opening-list");
      data.openings.forEach(function (element) {
        var div = $("<div>").append(
          $("<h2>").text(element.role),
          $("<strong>").text(element.openings + " Openings"),
          $("<a>")
            .addClass("small-bu")
            .text("Apply")
            .on("click", function () {
              if (element["redirect-url"] == "challenge") {
                showChallenge();
              } else {
                applyNow();
              }
            })
        );
        openings_div.append(div);
      });
    }, "./current-openings.json");
  }

  function showChallenge() {
    $("#challenge-now, #challenge").show();
    $("#apply-now").hide();
    $("body").addClass("hide-scroll");
    $("#popupclose").on("click", hideChallenge);
  }

  function hideChallenge() {
    $("#challenge, #apply-now, #challenge-now").hide();
    $("body").removeClass("hide-scroll");
    grecaptcha.reset()
  }

  function applyNow() {
    $("#apply-now").show();
    $("body").addClass("hide-scroll");
    $("#challenge-now").hide();
    $("#challenge").show();
    $("#popupclose").on("click", hideChallenge);
  }

  if (location.pathname.endsWith("/")) {
    requestJson(function (data) {
      var caseStudies_ul = $("#case-studies");
      data["case-studies"].forEach(function (element) {
        var li = $("<li>").append($("<img>").attr("src", element["image-url"]));
        caseStudies_ul.append(li);
      });
    }, "./case-studies.json");
  }
});

function showLoader() {
  document.getElementById('loader').style.display = 'block';
}

function hideLoader() {
  document.getElementById('loader').style.display = 'none';
}

function fetchConfigAndUpload(formData, mode, gresponse, queryStringParam) {
  showLoader();
  fetch("./config.json")
    .then((response) => response.json())
    .then((config) => {
      var url =
        config.upload_url +
        `?role=${
          mode === "dev" ? "dev" : "ui-dev"
        }&gresponse=${gresponse}` + queryStringParam;
      formData.append("role", mode === "dev" ? "dev" : "ui-dev");
      formData.append("gresponse", gresponse);

      return fetch(url, {
        method: "POST",
        body: formData,
      });
    })
    .then((response) => response.json())
    .then((res) => {
      var errorElement = document.getElementById(mode +"-form_error");
      var successElement = document.getElementById(mode + "-form_success");
      if (res.Error) {
        successElement.style.display = "none";
        errorElement.style.display = "block";
        errorElement.innerHTML = res.Error;
      } else {
        errorElement.style.display = "none";
        successElement.style.display = "block";
      }
    })
    .catch((error) => {
      console.error("Upload failed:", error);
    })
    .finally(()=> {
      hideLoader()
    });
}

function recaptchaCallback(id, mode) {
  var successMsg = document.getElementById(id + "-form_success");
  var errorMsg = document.getElementById(id + "-form_error");
  if (successMsg) successMsg.style.display = "none";
  if (errorMsg) errorMsg.style.display = "none";

  var form = document.getElementById(id);
  if (!form) {
    console.error("Form not found: ", id);
    return false;
  }

  var gResponse = form
    .querySelector("[name='g-recaptcha-response']")
    .value.trim();
  if (!gResponse) {
    errorMsg.textContent = "Captcha verification is required";
    errorMsg.style.display = "block";
    return false;
  }

  Upload(id, mode, gResponse);
}

function contactus(event) {
  var successMsg = document.getElementById("contact-form_success");
  var errorMsg = document.getElementById("contact-form_error");
  successMsg.style.display = "none";
  errorMsg.style.display = "none";

  var form = document.getElementById("contact-us-form");
  var formData = new FormData(form);
  var postData = {};

  for (var [key, value] of formData.entries()) {
    if (!value.trim()) {
      errorMsg.textContent = `${key} is required`;
      errorMsg.style.display = "block";
      return;
    }
    postData[key] = value;
  }

  var gresponse = formData.get("g-recaptcha-response");
  if (!gresponse) {
    errorMsg.textContent = "Captcha verification is required";
    errorMsg.style.display = "block";
    return;
  }
  showLoader();
  fetch("./config.json")
    .then((response) => response.json())
    .then((config) => {
      var url = `${config["contact-us-url"]}?gresponse=${gresponse}`;
      return fetch(url, {
        method: "POST",
        body: JSON.stringify(postData),
      });
    })
    .then((response) => response.json())
    .then((res) => {
      if (res.Error) {
        errorMsg.textContent = res.Error;
        errorMsg.style.display = "block";
      } else {
        successMsg.textContent = "Email sent successfully!";
        successMsg.style.display = "block";
      }
    })
    .catch((error) => {
      console.error("Contact form submission failed:", error);
      errorMsg.textContent = "An error occurred. Please try again.";
      errorMsg.style.display = "block";
    })
    .finally(()=>{
      hideLoader()
    });
}

function menuClick() {
  const bodyClassName = document.body.className;
  if (bodyClassName.includes(" showMenu")) {
    document.body.className = bodyClassName.split(" ")[0];
  } else {
    document.body.className = bodyClassName + " showMenu";
  }
}

function Upload(id, mode, gresponse) {
  var devMode = mode =="dev"
  var formErrorSelector = devMode ? "dev-form_error" : "non-dev-form_error";
  var fields = devMode
    ? ["answer", "name", "email"]
    : ["name", "email"];
  var queryStringParam = "&"
  var formData = new FormData();

  for (var i = 0; i < fields.length; i++) {
    var value = document.getElementById(mode + fields[i]).value.trim();
    if (!value) {
      document.getElementById(formErrorSelector).innerHTML =
        fields[i] + " is required";
      document.getElementById(formErrorSelector).style.display = "block";
      return false;
    }
    queryStringParam += `${fields[i]}=${value}&`
  }

  var file = document.getElementById(id + "-resume").files[0];
  if (file.size > 5 * 1024 * 1024) {
    document.getElementById(id + "-form_error").innerHTML =
      "File should be less than 5 MB";
    document.getElementById(id + "-form_error").style.display = "block";
    return false;
  }
  formData.append("file", file);

  fetchConfigAndUpload(formData, mode, gresponse, queryStringParam);
}
